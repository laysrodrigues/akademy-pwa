## Akademy PWA powered by [Quasar](https://quasar-framework.org)

### About [Akademy](https://akademy.kde.org):
For most of the year, KDE—one of the largest free and open software communities in the world—works on-line by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those that are looking for opportunities.


### To-Do

- [] - Setup for Akademy 2019

### For development

- Install the latest version of NodeJs, you can use [N](https://github.com/tj/n) to manage your NodeJs versions
- Install [Yarn](https://yarnpkg.com/en/docs/install#debian-stable)

```bash
yarn install
quasar dev
```
- Get acquaintance of how [quaser-cli](https://quasar-framework.org/guide/quasar-cli.html) works
- Read the [Quasar Docs](https://quasar-framework.org/components/)

### For deploy

This project has an automatic deploy to Gitlab pages.

This can be further improved to deploy in a specific server.
