
const routes = [
  {
    path: '/',
    component: () => import('layouts/main.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/:day',
        component: () => import('layouts/day.vue'),
        props: true
      },
      {
        path: '/bof/:day',
        component: () => import('layouts/bof.vue'),
        props: true
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
