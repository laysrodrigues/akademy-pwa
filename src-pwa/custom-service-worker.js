/*
 * This file (which will be your service worker)
 * is picked up by the build system ONLY if
 * quasar.conf > pwa > workboxPluginMode is set to "InjectManifest"
 */

if(workbox){
  console.log('Workbox loaded')
  workbox.routing.registerRoute(
    new RegExp('https://conf.kde.org/en/Akademy2018/public/schedule.json'),
    workbox.strategies.staleWhileRevalidate()
  )
}
